import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

const { DataTypes } = Sequelize;

const Users = db.define('master_users',{
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    full_name: {
        type: DataTypes.STRING(255),
        allowNull: true,
    },
    user_name: {
        type: DataTypes.STRING(150),
        allowNull: true,
    },
    password: {
        type: DataTypes.STRING(255),
        allowNull: true,
    },
    refresh_token: {
        type: DataTypes.TEXT,
        allowNull: true,
    },
    deleted: {
        type: DataTypes.ENUM('Y', 'N'),
        allowNull: true,
        defaultValue: 'N',
    },
    created_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    updated_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
},{
    freezeTableName:true
}
);

// ** Login Function
Users.login = async function(request) {
    const user = await Users.findOne({
        where:{
            user_name: request.username
        }
    });
    if (!user) throw Error('Username or Password not match in our server!!');
    const match = await bcrypt.compare(request.password, user.password);
    if(!match) throw Error('Username or Password not match in our server!!');
    if(user.deleted == 'Ya') throw Error('Your account has been deleted, please contact the admin immediately!!');
    const userId = user.id;
    const name = user.full_name;
    const username = user.user_name;
    const accessToken = jwt.sign({userId, name, username}, process.env.ACCESS_TOKEN_SECRET,{
        expiresIn: process.env.ACCESS_TOKEN_EXPIRED
    });
    const refreshToken = jwt.sign({userId, name, username}, process.env.REFRESH_TOKEN_SECRET,{
        expiresIn: process.env.REFRESH_TOKEN_EXPIRED
    });
    await Users.update({refresh_token: refreshToken},{
        where:{
            id: userId
        }
    });
    return {accessToken : accessToken,  refreshToken: refreshToken, userData: user }
};

Users.signup = async function(request) {
    const checkUsername = await Users.findOne({ where:{ user_name: request.username } });
    if(checkUsername){
        return {status: false, msg: 'Username has been taken!!!' };
    }

    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(request.password, salt);
    
    await Users.create({
        full_name: request.fullname,
        user_name: request.username,
        password: hashPassword,
    });

    return {status: true, msg: 'Registration Successfully!!!'}
};


export default Users;