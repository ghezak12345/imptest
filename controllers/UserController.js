
import Users from "../models/User.js";
import { getPagination } from "../utils/pagination.js";


export const getUsers = async(req, res) => {
    try {
        let { page, perPage, q, sort, sortColumn } = req.body;

        page = (!page ? 1 : page );
        perPage = (!perPage ? 10 : perPage );
        
        let { limit, offset } = getPagination(page, perPage);
        
        var condition = { deleted: 'N' }

        if(q){
            condition = Object.assign(condition, { 
                full_name: { [Op.like]: `%${q}%` } 
            });
        }

        sortColumn = (!sortColumn ? 'updated_at' : sortColumn );
        sort = (!sort ? 'desc' : sort );

        const users = await Users.findAll({
            order: [
                [sortColumn, sort]
            ],
            limit: limit,
            offset: offset,
            where: condition, // conditions
        });

        const total = await Users.count({
            where: condition, // conditions
        })
        
        res.json({users : users, total : total});
    } catch (error) {
        res.status(500).json({msg:"Server Error please try again later!!! "});
    }
}