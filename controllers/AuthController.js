import Users from "../models/User.js";
import jwt from "jsonwebtoken";

export const Login = async(req, res) => {
    try {
        const { username, password } = req.body;
        if (!username || username.length < 2) {
            return res.status(400).json({msg: "Check you username again"});
        }
        
        if (!password || password.length < 5) {
            return res.status(400).json({msg: "Check you password again"});
        }
        const user = await Users.login(req.body);
        res.cookie('refreshToken', user.refreshToken,{
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        });
        res.cookie('accessToken', user.accessToken,{
            httpOnly: true,
            maxAge: 1 * 60 * 60 * 1000
        });
        res.status(200).json({ accessToken : user.accessToken,  refreshToken: user.refreshToken, userData: {"full_name" : user.userData.full_name, "user_name" : user.userData.user_name } });
    } catch (error) {
        res.status(500).json({msg:error.message});
    }
}

export const SignUp = async(req, res) => {
    try {
        const { username, password } = req.body;
        if (!username || username.length < 2) {
            return res.status(400).json({msg: "Check you username again"});
        }
        
        if (!password || password.length < 5) {
            return res.status(400).json({msg: "Check you password again"});
        }
        const singUp = await Users.signup(req.body);
        if(singUp.status){
            res.json({ success: singUp.status, msg: singUp.msg });
        }else{
            res.status(409).json({ msg: singUp.msg });
        }
    } catch (error) {
        res.status(500).json({msg:"Server Error please try again later!!! "});
    }
}

export const refreshToken = async(req, res) => {
    try {
        const refreshToken = req.cookies.refreshToken;
        let token = null;
        if(refreshToken){
            token = refreshToken;
        }else{
            const authHeader = req.headers['authorization'];
            token = authHeader && authHeader.split(' ')[1];
        }
        if(!token) return res.sendStatus(401);
        const user = await Users.findOne({
            where:{
                refresh_token: token
            }
        });
        if(!user) return res.sendStatus(401);
        jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, (err) => {
            if(err) return res.sendStatus(401);
            const userId = user.id;
            const name = user.full_nme;
            const username = user.user_name;
            const accessToken = jwt.sign({userId, name, username}, process.env.ACCESS_TOKEN_SECRET,{
                expiresIn: process.env.ACCESS_TOKEN_EXPIRED
            });
            res.cookie('accessToken', accessToken,{
                httpOnly: true,
                maxAge: 1 * 60 * 60 * 1000
            });
            res.json({ accessToken : accessToken,  refreshToken: token, userData: {"fullName" : user.full_name, "vUsername" : user.user_name } });
        });
    } catch (error) {
        throw error;
    }
}

export const Logout = async(req, res) => {
    const refreshToken = req.cookies.refreshToken;
    if(!refreshToken) return res.sendStatus(204);
    const user = await Users.findOne({
        where:{
            refresh_token: refreshToken
        }
    });
    if(!user) return res.sendStatus(204);
    const userId = user.id;
    await Users.update({refresh_token: null},{
        where:{
            id: userId
        }
    });
    res.clearCookie('refreshToken');
    res.clearCookie('accessToken');
    return res.sendStatus(200);
}