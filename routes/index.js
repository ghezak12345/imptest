// ** Navigation imports
import authenticate from './route/authenticate.js'
import users from './route/users.js'

// ** Merge & Export
export default [authenticate, users]
