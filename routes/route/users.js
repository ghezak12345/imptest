import express from "express";
import { getUsers } from "../../controllers/UserController.js";
import { verifyToken } from "../../middleware/VerifyTOken.js";

const router = express.Router();

router.use(verifyToken)
router.post('/user/userList', getUsers);

export default router;