import express from "express";
import { Login, Logout, SignUp, refreshToken } from "../../controllers/AuthController.js";

const router = express.Router();

router.post('/auth/login', Login);
router.post('/auth/signup', SignUp);
router.get('/auth/token', refreshToken);
router.get('/auth/logout', Logout);

export default router;