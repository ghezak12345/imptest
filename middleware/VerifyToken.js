import jwt from "jsonwebtoken";

export const verifyToken = (req, res, next) => {

    const accessToken = req.cookies.accessToken;
    let token = null;
    if(accessToken){
        token = accessToken;
    }else{
        const authHeader = req.headers['authorization'];
        token = authHeader && authHeader.split(' ')[1];
    }
    if(token == null) return res.sendStatus(401);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
        if(err) return res.sendStatus(401);
        req.userId = decoded.userId;
        next();
    })
}