import { Sequelize } from "sequelize";
import dotenv from "dotenv";
import Users from "../models/User.js";
import bcrypt from "bcrypt";

dotenv.config();

const db = new Sequelize(process.env.DBNAME, process.env.DBUSER, process.env.DBPASSWORD, {
    host: process.env.DBHOST,
    dialect: "mysql",
    define: {
        timestamps: false
    },
    // logging: false
});

// Migrate the database
db.sync() // set force to true to drop existing tables and recreate them
.then(() => {
    console.log('Database migrated successfully.');
    return seedDatabase();
})
.catch((error) => {
    console.error('Error migrating database:', error);
});

async function seedDatabase() {
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash("asdqwe123", salt);
    // Define your initial data here as JavaScript objects
    const users = [
        { 
            full_name: 'test', 
            user_name: 'test' ,
            password: hashPassword,
        }
    ];

    // Create records for the initial data using Sequelize
    const createdUsers = await Users.bulkCreate(users);

    console.log('Database seeded successfully with', createdUsers.length, 'users.');
}

export default db;